#ifndef DESK_SHORT_H
#define DESK_SHORT_H
#include<iostream>
#include<iomanip>
#include<stdlib.h>
#include<fstream>
#include<map>
#include<string>
#include<vector>
using namespace std;

const string APPLICATION_NAME = "./DeskShort";
const string FIRST_LINE = "[Desktop Entry]";
const string NAME_LINE = "Name=";
const string COMMENT_LINE = "Comment=";
const string EXEC_LINE = "Exec=";
const string ICON_LINE = "Icon=";
const string TERMINAL_LINE = "Terminal=false";
const string TYPE_LINE = "Type=Application";
// relative path to desktop
const string FILE_PATH = string(getenv("HOME")) + "/Desktop/";

// indices for the outputs vector
const int 
  NAME_INDEX = 0,
  COMMENT_INDEX = 1,
  EXEC_INDEX = 2,
  ICON_INDEX = 3,
  NUM_INDICES = 4;
  

class DeskShort{
 private:
  map<string,string> inputFlags; // container for user input
  vector<string> outputs; // appended to the output strings
  ofstream outFile; // outputs to a file
  ifstream checkFile; // used to check if the application file exists

  string getExecName();
  
 public:
  DeskShort();
  void processInput(int argc, char** args);
  void writeFile();
  ~DeskShort();
  
};
#endif

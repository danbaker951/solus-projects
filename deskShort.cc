#include"deskShort.h"

int main(int argc, char** args){
  if(argc < 2){
    cout << "Invalid input!" << endl;
  }
  else{  
    DeskShort shortcutCreator;
    shortcutCreator.processInput(argc, args);
    shortcutCreator.writeFile();
  }
  return 0;
}

DeskShort::DeskShort(){
  // make cases for input options
  inputFlags["-c"] = ""; // add a comment
  inputFlags["-i"] = ""; // add a custom icon
  inputFlags["-n"] = ""; // add a custom name
  outputs.resize(NUM_INDICES); // reserve four output strings
} // constructor

void DeskShort::processInput(int argc, char** args){
  outputs[EXEC_INDEX] = args[1]; // second argument is always the Exec path

  if(argc > 2){
    string currentFlag = args[2]; // get the first flag
    if(inputFlags.find(currentFlag) == inputFlags.end()){
	cout << "Invalid argument: " << currentFlag << ". Valid flags are -c -i and -n";
	exit(1);
      } // this if statement occurs if the first argument after the path is not a valid flag
    // loop through the remaining input arguments
    for(int i = 3; i < argc; i++){
      if(inputFlags.find(args[i]) != inputFlags.end())
	currentFlag = args[i]; // this if statement occurs if a new flag has been found
      else{
	inputFlags[currentFlag] += args[i]; // add the argument to the current flag
	inputFlags[currentFlag] += " ";
      }
    } 
  }
  // assign the inputs to the proper outputs
  outputs[COMMENT_INDEX] = inputFlags["-c"];
  outputs[NAME_INDEX] = inputFlags["-n"];
  outputs[ICON_INDEX] = inputFlags["-i"];

  // if there was no input for name or icon, call them whatever the path is
  if(outputs[NAME_INDEX].compare("") == 0)
    outputs[NAME_INDEX] = getExecName();
  if(outputs[ICON_INDEX].compare("") == 0)
    outputs[ICON_INDEX] = getExecName();
} // end processInput

void DeskShort::writeFile(){
  ifstream checkFile(outputs[EXEC_INDEX]);
  if(!checkFile){
    cout << "Invalid executable path!" << endl;
    exit(2);
  }
  else{
    string outputFile = FILE_PATH + outputs[NAME_INDEX] + ".desktop";
    outFile.open(outputFile);
    if(outFile.fail()){
      cout << "failed to create " << outputFile << endl;
      exit(3);
    }
    outFile << FIRST_LINE << endl
	    << NAME_LINE << outputs[NAME_INDEX] << endl
	    << COMMENT_LINE << outputs[COMMENT_INDEX] << endl
	    << EXEC_LINE << outputs[EXEC_INDEX] << endl
	    << ICON_LINE << outputs[ICON_INDEX] << endl
	    << TERMINAL_LINE << endl
	    << TYPE_LINE << endl;
    outFile.close();
    string command = "chmod 755 \"" + outputFile + "\"";
    system(command.c_str());
  }
} // end writeFile

string DeskShort::getExecName(){
  string returnString = outputs[EXEC_INDEX];
  int index = returnString.find('/');
  // while an instance of '/' exists
  while(index < returnString.size()){
    returnString = returnString.substr(index + 1, returnString.size()); // get the string after the '/'
    index = returnString.find('/'); // get the next index
  }
  return returnString;
} // end getExecName

DeskShort::~DeskShort(){}


